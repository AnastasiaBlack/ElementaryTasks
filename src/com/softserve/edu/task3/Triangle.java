package com.softserve.edu.task3;

public class Triangle implements Comparable<Triangle> {
    private String name;
    private double sideA;
    private double sideB;
    private double sideC;
    private double square;

    /**
     * The constructor validates whether such a Triangle can exist, and if no -
     * throws an exception.
     * 
     * @param inputName
     *            name of the triangle
     * @param inputA
     *            side a of the triangle
     * @param inputB
     *            side b of the triangle
     * @param inputC
     *            side c of the triangle
     */
    public Triangle(String inputName, double inputA, double inputB,
            double inputC) {
        this.name = inputName;
        this.sideA = inputA;
        this.sideB = inputB;
        this.sideC = inputC;
        if (!isTriangleValid()) {
            throw new IllegalArgumentException("The triangles with such sides "
                    + "can't exist. Each side should be smaller than the sum of "
                    + "two other ones");
        }
        countSquareHeron();

    }

    /**
     * Counts a square according to the Heron's formula.
     */
    private void countSquareHeron() {
        double halfP = (sideA + sideB + sideC) / 2;
        double mult = halfP * (halfP - sideA) * (halfP - sideB)
                * (halfP - sideC);
        square = Math.sqrt(mult);
    }

    /**
     * Validates the length of the sides of the triangles.
     * 
     * @return
     */
    private boolean isTriangleValid() {
        if (sideA < (sideB + sideC) && sideB < (sideA + sideC)
                && sideC < (sideA + sideB)) {
            return true;
        }
        return false;
    }

    public int compareTo(Triangle o) {
        if (this.square > o.square) {
            return -1;
        } else if (this.square < o.square) {
            return 1;
        }
        return 0;
    }

    @Override
    public String toString() {
        return "[Triangle " + name + "]: " + square + " cm";
    }

    public double getSquare() {
        return square;
    }

    public void setSqare(double newSquare) {
        this.square = newSquare;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSideA() {
        return sideA;
    }

    public void setSideA(double sideA) {
        this.sideA = sideA;
    }

    public double getSideB() {
        return sideB;
    }

    public void setSideB(double sideB) {
        this.sideB = sideB;
    }

    public double getSideC() {
        return sideC;
    }

    public void setSideC(double sideC) {
        this.sideC = sideC;
    }

}
