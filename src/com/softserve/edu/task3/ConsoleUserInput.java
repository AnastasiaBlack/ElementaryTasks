package com.softserve.edu.task3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

import com.softserve.edu.task1.IncorrectInputFormateException;

public class ConsoleUserInput implements UserInputInterface {
    /**
     * The general method that combines the whole logics. It interacts with the
     * user, asks whether they want to continue.
     * 
     */

    public void launch() {
        final int indexName = 0;
        final int indexSideA = 1;
        final int indexSideB = 2;
        final int indexSideC = 3;
        List<Triangle> triangles = new ArrayList<Triangle>();
        Triangle tr;
        Scanner sc = new Scanner(System.in);
        boolean active;

        do {
            active = false;
            System.out.println("Please, enter the name of a triangle and its "
                    + "sides in centimeters, separating them by commas, like "
                    + "this: My triangle,12,14,16");
            String userInput = sc.nextLine();
            try {
                String[] userArr;
                userArr = parseString(userInput);
                tr = createTriangle(userArr[indexName], userArr[indexSideA],
                        userArr[indexSideB], userArr[indexSideC]);
                triangles.add(tr);
            } catch (IncorrectInputFormateException e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
            System.out.println("Do you want to enter one more Triangle?Type y"
                    + " or yes if you do.");
            userInput = sc.nextLine();
            if (userInput.equalsIgnoreCase("y")
                    || userInput.equalsIgnoreCase("yes")) {
                active = true;
            } else {
                Collections.sort(triangles, new TrianglesSort());
                for (Triangle t : triangles) {
                    System.out.println(t.toString());
                }
            }
        } while (active);
        sc.close();
        return;

    }

    /**
     * We encapsulate the creation of a triangle, because we need to convert the
     * Strings into doubles, leaving the first String for name in the String
     * type.
     * 
     */
    public Triangle createTriangle(String trName, String a, String b,
            String c) {
        double sideA = Double.valueOf(a);
        double sideB = Double.valueOf(b);
        double sideC = Double.valueOf(c);
        Triangle userTriangle = new Triangle(trName, sideA, sideB, sideC);
        return userTriangle;
    }

    /**
     * Validates the input string whether it's null or the arguments were
     * written incorrectly (4 arguments separated by commas needed).
     * 
     * @param str
     *            a string entered by the user
     * @return an array with 4 elements
     * @throws IncorrectInputFormateException
     */
    public String[] parseString(String str)
            throws IncorrectInputFormateException {
        if (str.equals("")) {
            throw new IncorrectInputFormateException(
                    "You have entered an empty line");
        }
        String[] arrStr;
        String reg = "(\\s*||\\t*),(\\s*||\\t*)";
        arrStr = str.toLowerCase().split(reg);
        if (arrStr.length != 4) {
            throw new IncorrectInputFormateException("Oops! It seems you've "
                    + "made a mistake. Please, check your input.");
        }
        return arrStr;
    }

}
