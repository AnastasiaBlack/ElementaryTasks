package com.softserve.edu.task3;

import java.util.Comparator;

public class TrianglesSort implements Comparator<Triangle> {

    public int compare(Triangle o1, Triangle o2) {
        return o1.compareTo(o2);
    }
}
