package com.softserve.edu.task3.tests;

import org.junit.Assert;
import org.testng.annotations.Test;

import com.softserve.edu.task1.IncorrectInputFormateException;
import com.softserve.edu.task3.ConsoleUserInput;

public class ConsoleUserInputTest {

    @Test
    public void parseString_ValidInputString_ArrayExpected() {
        // Arrange
        try {
            String userInput = "TestTriangle, 12,   13,16";
            String[] expectedArr = { "testtriangle", "12", "13", "16" };
            ConsoleUserInput ui = new ConsoleUserInput();
            // Act

            String[] actualArr;
            actualArr = ui.parseString(userInput);

            // Assert
            Assert.assertArrayEquals(expectedArr, actualArr);
        } catch (IncorrectInputFormateException e) {
            e.printStackTrace();
        }
    }

    @Test(expectedExceptions = IncorrectInputFormateException.class)
    public void parseString_InvalidInputString_IncorrectInputFormateExceptionThrown()
            throws IncorrectInputFormateException {
        // Arrange
        String userInput = "q,s ,12,,13,16";
        ConsoleUserInput ui = new ConsoleUserInput();
        // Act
        try {
            ui.parseString(userInput);
        } finally {
        }
        // Assert
    }
}