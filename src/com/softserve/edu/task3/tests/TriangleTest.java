package com.softserve.edu.task3.tests;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.softserve.edu.task3.Triangle;

public class TriangleTest {

    @Test
    public void Triangle_NotNullNameValidSides_SquareCalculated() {
        // Arrange
        String name = "Triangle 1";
        double a = 5;
        double b = 6;
        double c = 7;
        double p = (a + b + c) / 2;
        double square = Math.sqrt(p * (p - a) * (p - b) * (p - c));
        Triangle triangle = new Triangle(name, a, b, c);

        // Act
        double actualSquare = triangle.getSquare();

        // Assert
        Assert.assertEquals(square, actualSquare);

    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void Triangle_NotNullNameSidesNotValid_IllegalArgumentExceptionThrown() {
        // Arrange
        String name = "Triangle 1";
        double a = 5;
        double b = 2;
        double c = 7;

        // Act
        new Triangle(name, a, b, c);
    }

    @Test
    public void Sort_DescendingOrderExpected() {
        // Arrange
        Triangle tr1 = new Triangle("Triangle 1", 5, 3, 7);
        Triangle tr2 = new Triangle("Triangle 2", 1, 2, 2);
        Triangle tr3 = new Triangle("Triangle 3", 15, 10, 20);
        Triangle[] trianglesExpected = {tr3,tr1,tr2};
        ArrayList<Triangle> trEx= new ArrayList<Triangle>(Arrays.asList(trianglesExpected));
        Triangle[] trianglesActual = {tr1,tr2,tr3};
        ArrayList<Triangle> trActual = new ArrayList<Triangle>(Arrays.asList(trianglesActual));

        // Act
        Collections.sort(trActual);

        // Assert
        Assert.assertEquals(trActual, trEx);
    }

}
