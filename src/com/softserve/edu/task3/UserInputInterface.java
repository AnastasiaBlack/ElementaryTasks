package com.softserve.edu.task3;

public interface UserInputInterface {
    /**The general method that combines the whole logics.
     * It interacts with the user, asks whether they want to continue.
     * 
     */
    public void launch();
    
    /**User enters a name and all the sides of a triangle. All string-sides are
     * converted to double. A method then creates an object (while validating 
     * whether the object exists in the Triangle constructor).
     * 
     * @param trName
     * @param a
     * @param b
     * @param c
     * @return Triangle
     */
    public Triangle createTriangle(String trName, String a, String b, String c);
    

}
