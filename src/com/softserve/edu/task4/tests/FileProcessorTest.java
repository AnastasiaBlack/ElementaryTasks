package com.softserve.edu.task4.tests;

import java.io.IOException;
import java.security.InvalidParameterException;

import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.Test;

import com.softserve.edu.task4.FileProcessor;

public class FileProcessorTest {
    @Mock
    private FileProcessor fp;

    @Test(expectedExceptions = IOException.class)
    public void ReplaceString_ExpectedExeptionThrown() throws IOException {
        //Arrange
        MockitoAnnotations.initMocks(this);
        Mockito.doThrow(IOException.class).when(fp).replaceString();
        //Act
        fp.replaceString();
        //Assert
    }
    
    @Test(expectedExceptions = InvalidParameterException.class)
    public void ConstructorFileProcessor_ExpectedExeptionThrown(){
        //Arrange
        String[] wrongArgs = new String[0];
        
        //Act
        FileProcessor fp = new FileProcessor(wrongArgs);
        
    }

}
