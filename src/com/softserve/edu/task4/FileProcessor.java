package com.softserve.edu.task4;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.security.InvalidParameterException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FileProcessor {
    private File file;
    private String wholeText;
    private Pattern pattern;
    private Matcher matcher;
    private String filePath;
    private String searchedString;
    private String replacementString;

    public FileProcessor(String[] args) {
        if(args.length<1){
            System.out.println("Please, enter the arguments");
            throw new InvalidParameterException("Please, enter the arguments");
        }
        this.filePath = args[0];
        this.searchedString = args[1];
        if (args.length > 2) {
            this.replacementString = args[2];
        }

    }

    /**
     * Reads the file and creates a string, containing the whole text.
     * 
     * @throws IOException
     */
    public void readWholeText() {
        file = new File(filePath);
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"))) {
            StringBuilder builder = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                builder.append(line);
            }
            wholeText = builder.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Counts and prints out the number of occurrences of the String specified
     * by a user during the launch of the app
     * 
     * @throws IOException
     */
    public void countOccurence() throws IOException {
        readWholeText();
        pattern = Pattern.compile(searchedString);
        matcher = pattern.matcher(wholeText);
        int count = 0;
        while (matcher.find()) {
            count++;
        }
        System.out.println("\"" + searchedString + "\" occurs " + count + " times");
    }

    /**
     * Replaces the predefined string with a new one (specified by a user as
     * well) and writes the new text with changes into the file.
     * 
     * @throws IOException
     */
    public void replaceString() throws IOException {
        readWholeText();
        BufferedWriter bw;
        String replaced = wholeText.replace(searchedString, replacementString);
        bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
        bw.write(replaced);
        System.out.println(searchedString + " is changed to " + replacementString);
        bw.close();
    }

    
}
