package com.softserve.edu.task2;

public class Envelope {
	private double oneSide;
	private double otherSide;

	public Envelope(double side1, double side2) {
		oneSide = side1;
		otherSide = side2;
	}
	
	/**
	 * returns the smallest side of an envelope
	 * @return
	 */
	public double getMinSide(){
		return Math.min(oneSide, otherSide);
	}
	
	public double getMaxSide(){
		return Math.max(oneSide, otherSide);
	}
	

}
