package com.softserve.edu.task2.tests;

import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.softserve.edu.task2.UserInput;

public class UserInputTest {
    @Mock
    UserInput ui;

    @Test(expected = IllegalArgumentException.class)
    public void ValidateInput_ExpectedIllegalArgumentExceptionThrown() {
        MockitoAnnotations.initMocks(this);

        String wrongInput = "32t4eq2";
        
        ui.validateInput(wrongInput);
    }

}
