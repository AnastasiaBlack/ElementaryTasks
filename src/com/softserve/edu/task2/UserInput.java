package com.softserve.edu.task2;

import java.util.Scanner;

public class UserInput {
    private double heightA;
    private double widthB;
    private double heightC;
    private double widthD;
    private Scanner sc;
    private boolean active;

    public UserInput(Scanner sc) {
        this.sc = sc;
    }

    /**
     * Asks a user to enter one parameter at a time, validates the input
     * 
     * @throws Exception
     */
    public void startUsertInput() throws Exception {
        String side;
        System.out.println("Please, enter a paramater A:");
        side = sc.nextLine();
        validateInput(side);
        heightA = Integer.parseInt(side);
        System.out.println("Please, enter a paramater B:");
        side = sc.nextLine();
        validateInput(side);
        widthB = Integer.parseInt(side);
        System.out.println("Please, enter a paramater C:");
        side = sc.nextLine();
        validateInput(side);
        heightC = Integer.parseInt(side);
        System.out.println("Please, enter a paramater D:");
        side = sc.nextLine();
        validateInput(side);
        widthD = Integer.parseInt(side);

    }

    /**
     * Checks if the input String is null or not a number.
     * 
     * @param a
     * @throws Exception
     *             is supported with detailed message
     */
    public void validateInput(String a) throws IllegalArgumentException {
        if (a.equals("")) {
            throw new IllegalArgumentException(
                    "!!!Enter the height and the width of the chess-board and it will be printed out in the console."
                            + "Type \"stop\" to terminate the app \n!!!");

        }
        String regX = "\\d*.\\d*";
        if (!a.matches(regX)) {
            throw new IllegalArgumentException(
                    "!!! Uh-oh!)Incorrect input formate. Please enter the numbers only !!!");
        }
    }

    /**
     * Asks a user whether he wants to continue and if yes sets a field "active"
     * as true.
     */
    public void wantContinue() {
        System.out.println(
                "Do you want to proceed with two more envelopes?Type y or yes if you do.");
        String answer = sc.nextLine();
        if (answer.equals("y") || answer.equalsIgnoreCase("yes")) {
            active = true;
        }
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public double getHeightA() {
        return heightA;
    }

    public void setHeightA(double heightA) {
        this.heightA = heightA;
    }

    public double getWidthB() {
        return widthB;
    }

    public void setWidthB(double widthB) {
        this.widthB = widthB;
    }

    public double getHeightC() {
        return heightC;
    }

    public void setHeightC(double heightC) {
        this.heightC = heightC;
    }

    public double getWidthD() {
        return widthD;
    }

    public void setWidthD(double widthD) {
        this.widthD = widthD;
    }

}
