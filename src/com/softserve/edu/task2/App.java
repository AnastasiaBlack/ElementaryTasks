package com.softserve.edu.task2;

import java.util.Scanner;

public class App {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		UserInput user = new UserInput(sc);
		PuttingEnvelopesService putService = new PuttingEnvelopesService(user);
		putService.start();
		sc.close();

	}

}
