package com.softserve.edu.task2;

public class PuttingEnvelopesService {
    private UserInput userInput;
    private Envelope envelopeAB;
    private Envelope envelopeCD;

    public PuttingEnvelopesService(UserInput input) {
        userInput = input;
    }

    /**
     * Launches the procedure of user input and prints out the result. Uses
     * a boolean value, to define whether a user wants to continue or the app can be closed.  
     */

    public void start() {
        do {
            try {
                userInput.setActive(false);
                userInput.startUsertInput();
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            createEnvelopes();
            if (canPut()) {
                System.out.println("You can put one envelope to another");
            } else {
                System.out.println("Sorry, you can't put one envelope into another");
            }
            userInput.wantContinue();
        } while (userInput.isActive());

    }

    /**
     * Creates envelopes, according to the input data.
     */
    public void createEnvelopes() {
        double a = userInput.getHeightA();
        double b = userInput.getWidthB();
        double c = userInput.getHeightC();
        double d = userInput.getWidthD();
        envelopeAB = new Envelope(a, b);
        envelopeCD = new Envelope(c, d);
    }

    /**
     * Checks whether envelopes can be put into each other.
     * To put one envelope into another the smallest side of it should be smaller than the smallest side of
     * the other one, and the largest side should also be smaller than the largest side of the second envelope.
     * And we can't put it, if they are equal.
     * 
     * @return boolean
     */
    public boolean canPut() {
        double abMin = envelopeAB.getMinSide();
        double cdMin = envelopeCD.getMinSide();
        double abMax = envelopeAB.getMaxSide();
        double cdMax = envelopeCD.getMaxSide();
        if (abMin < cdMin && abMax < cdMax || cdMin < abMin && cdMax < abMax) {
            return true;
        }
        return false;
    }
}
