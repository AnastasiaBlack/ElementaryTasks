package com.softserve.edu.task5;

public class ZeroToTen extends DigitsDecorator {

    private String number = "";

    public ZeroToTen(NumberInterface newbaseNumber) {
        super(newbaseNumber);
        inputStr = super.inputStr;
    }

    public ZeroToTen() {
        inputStr = super.inputStr;
    }

    public ZeroToTen(String inputDigits) {
        inputStr = inputDigits;
    }

    @Override
    public String getWrittenNumber() {
        return defineNumber(super.inputStr);
    }

    public String getStartingNumber() {
        return defineNumber(super.inputStr);
    }
/**
 * Defines a number from 0-9.
 */
    public String defineNumber(String nr) {
        char[] charArr = nr.toCharArray();
        char ch = charArr[charArr.length - 1];
        if (charArr.length == 1) {
            number = " zero";
        }
        switch (ch) {
        case '1':
            number = " one";
            break;
        case '2':
            number = " two";
            break;
        case '3':
            number = " three";
            break;
        case '4':
            number = " four";
            break;
        case '5':
            number = " five";
            break;
        case '6':
            number = " six";
            break;
        case '7':
            number = " seven";
            break;
        case '8':
            number = " eight";
            break;
        case '9':
            number = " nine";
            break;
        }
        return number;
    }

    @Override
    public String getInputDigits() {
        // TODO Auto-generated method stub
        return null;
    }

}
