package com.softserve.edu.task5;

/*Establishes basic functionality for the core digit.
 * 
 */
public interface NumberInterface {
    public String getWrittenNumber();
    public String getInputDigits();
    

}
