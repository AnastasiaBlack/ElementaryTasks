package com.softserve.edu.task5;

import java.security.InvalidParameterException;

/**
 * Here we will define what is the biggest digit in our number (thousand,
 * hundred etc). It will be used as a core component for the decoration.
 * 
 * @author Анастасія
 *
 */
public class StartingNumber extends DigitsDecorator implements NumberInterface {
    private int length;
    private String inputDigits;
    static DigitsDecorator decorator = null;

    public StartingNumber(String inputNumber, int digits) {
        if (!inputNumber.matches("\\d*")) {
            throw new InvalidParameterException(
                    "Please, enter the numebers only");
        }
       
        inputDigits = inputNumber;
        /*
         * in future we may convert the input into integer type to validate the
         * data and the use int digits = String.valueOf(intNumber).length();
         */
        this.length = digits;
    }

    /**
     * This method will be called the first of the hierarchy, that's why it
     * defines the starting digit and returns a starting number.
     */
    @Override
    public String getWrittenNumber() {
        String startingNumber = "The app doesn't support a digit you've entered";
        Tens tens;
        Hundreds hundreds;
        Thousands thousands;
        
        switch (length) {
        case 1:
            decorator.getWrittenNumber();
        case 2:
            tens = new Tens();
            startingNumber = tens.getStartingNumber(inputDigits);
            break;
        case 3:
            hundreds = new Hundreds();
            startingNumber = hundreds.getStartingNumber(inputDigits);
            break;
        case 4:
            thousands = new Thousands();
            startingNumber = thousands.getStartingNumber(inputDigits);
            break;
        case 5:
            thousands = new Thousands();
            startingNumber = thousands.getStartingNumber(inputDigits);
            break;
        case 6:
            thousands = new Thousands();
            startingNumber = thousands.getStartingNumber(inputDigits);
            break;
        }

        return startingNumber;
    }

    /**
     * Returns the decorator according to the number of digits of the entered
     * number
     * 
     * @param startInputNumber
     * @return decorator
     */
    public static DigitsDecorator getDecoratorModel(String[] args)
            throws InvalidParameterException {

        if (args.length == 0) {
            System.out.println(
                    "Please, enter the number in parameters before launch");
            throw new InvalidParameterException();
        }     
        String startInputNumber = args[0];
        int digits = startInputNumber.length();
        if(digits>6){
            throw new InvalidParameterException("Sorry, your number is too long");
        }
        switch (digits) {
        case 1:
            decorator = new ZeroToTen(startInputNumber);
            break;
        case 2:
            decorator = new StartingNumber(startInputNumber, digits);
            decorator.setInputStr(startInputNumber);
            break;
        case 3:
            decorator = new Tens(new StartingNumber(startInputNumber, digits));
            decorator.setInputStr(startInputNumber);
            break;
        case 4:
            decorator = new Tens(
                    new Hundreds(new StartingNumber(startInputNumber, digits)));
            decorator.setInputStr(startInputNumber);
            break;
        case 5:
            decorator = new Tens(
                    new Hundreds(new StartingNumber(startInputNumber, digits)));
            decorator.setInputStr(startInputNumber);
            break;
        case 6:
            decorator = new Tens(
                    new Hundreds(new StartingNumber(startInputNumber, digits)));
            decorator.setInputStr(startInputNumber);
            break;
        }
        return decorator;
    }

    public String getInputDigits() {
        return inputDigits;
    }

    public void setInputDigits(String inputDigits) {
        this.inputDigits = inputDigits;
    }

}
