package com.softserve.edu.task5.tests;

import java.security.InvalidParameterException;

import org.mockito.Mock;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.softserve.edu.task5.DigitsDecorator;
import com.softserve.edu.task5.StartingNumber;

public class StartingNumberTest {
    @Mock
    private DigitsDecorator dec;

    @Test(expectedExceptions = InvalidParameterException.class)
    public void getDecorator_ExpectedInvalidParameterExceptionThrown() {
        // Arrange
        String[] wrongArgs = new String[0];
        // Act
        StartingNumber.getDecoratorModel(wrongArgs);
    }

    @Test
    public void getWrittenNumber_ExpectedCorrectWrittenNumber() {
        // Arrange
        String[] args = { "125443" };
        String expectedNumber = " one hundred twenty five thousand four hundred fourty three";
        // Act
        DigitsDecorator dec = StartingNumber.getDecoratorModel(args);
        String actualNumber = dec.getWrittenNumber();
        // Assert
        Assert.assertEquals(actualNumber, expectedNumber);
    }
    
    @Test(expectedExceptions = InvalidParameterException.class)
    public void StartingNumber_InvalidInput_ExpectedInvalidParameterExceptionThrown() {
        // Arrange
        String[] wrongArgs = {"83jd5"};
        // Act
        DigitsDecorator dec = StartingNumber.getDecoratorModel(wrongArgs);
        dec.getWrittenNumber();
        }
    
    @Test(expectedExceptions = InvalidParameterException.class)
    public void StartingNumber_TooLong_ExpectedInvalidParameterExceptionThrown() {
        // Arrange
        String[] wrongArgs = {"8344554325"};
        // Act
       StartingNumber.getDecoratorModel(wrongArgs);
        }

}
