package com.softserve.edu.task5;

public class Tens extends DigitsDecorator {
    private String number = " десятки";

    public Tens(NumberInterface newBaseNumber) {
        super(newBaseNumber);
        inputStr = newBaseNumber.getInputDigits();
    }

    public Tens() {
    }

    public String getStartingNumber(String input) {
        return defineNumber(input);
    }

    @Override
    public String getWrittenNumber() {
        return super.getWrittenNumber() + defineNumber(inputStr);
    }
/**
 * Defines numbers from 10-20
 * @return number
 */
    public String defineNumber(String nr) {
        ZeroToTen zer = new ZeroToTen();
        char[] charArr = nr.toCharArray();
        char ten = charArr[charArr.length - 2];
        char zerCh = charArr[charArr.length - 1];
        StringBuilder sb = new StringBuilder();
        sb.append(ten);
        sb.append(zerCh);
        String tempStr = sb.toString();

        int intNr = Integer.valueOf(tempStr);
        if (intNr >= 10 && intNr < 21) {
            switch (tempStr) {
            case "10":
                number = " ten";
                break;     
            case "11":     
                number = " eleven";
                break;     
            case "12":     
                number = " twelve";
                break;     
            case "13":     
                number = " thirteen";
                break;     
            case "14":     
                number = " fourteen";
                break;     
            case "15":     
                number = " fifteen";
                break;     
            case "16":     
                number = " sixteen";
                break;     
            case "17":     
                number = " seventeen";
                break;     
            case "18":     
                number = " eighteen";
                break;     
            case "19":     
                number = " nineteen";
                break;     
            case "20":
                number = " twenty";
                break;
            }
        } else {
            switch (ten) {
            case '0':
                number = "" + zer.defineNumber(String.valueOf(zerCh));
                break;
            case '2':
                number = " twenty" + zer.defineNumber(String.valueOf(zerCh));
                break;
            case '3':
                number = " thirty" + zer.defineNumber(String.valueOf(zerCh));
                break;
            case '4':
                number = " fourty" + zer.defineNumber(String.valueOf(zerCh));
                break;
            case '5':
                number = " fifty" + zer.defineNumber(String.valueOf(zerCh));
                break;
            case '6':
                number = " sixty"
                        + zer.defineNumber(String.valueOf(zerCh));
                break;
            case '7':
                number = " seventy" + zer.defineNumber(String.valueOf(zerCh));
                break;
            case '8':
                number = " eighty"
                        + zer.defineNumber(String.valueOf(zerCh));
                break;
            case '9':
                number = " ninety" + zer.defineNumber(String.valueOf(zerCh));
            }

        }
        return number;
    }

    @Override
    public String getInputDigits() {
        // TODO Auto-generated method stub
        return null;
    }
}
