package com.softserve.edu.task5;
/**Extend this class if you want to add another digit to your App (million,
 *  billions etc.).
 * 
 * @author Анастасія
 *
 */
public abstract class DigitsDecorator implements NumberInterface {
    protected NumberInterface tempBaseNumber;
    protected String inputStr;

    public DigitsDecorator(NumberInterface newbaseNumber) {
        tempBaseNumber=newbaseNumber;
    }
    public DigitsDecorator() {
    }

    
    public String defineNumber(String nr) { return nr; }
    
    @Override
    public String getWrittenNumber() {
        
        return tempBaseNumber.getWrittenNumber();
    }
    
    public void setInputStr(String str){
        inputStr=str;
    }

}
