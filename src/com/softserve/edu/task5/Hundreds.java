package com.softserve.edu.task5;

public class Hundreds extends DigitsDecorator {
    private String number = "hundreds";

    public Hundreds(NumberInterface newbaseNumber) {
        super(newbaseNumber);
        inputStr = newbaseNumber.getInputDigits();
    }

    public Hundreds() {
    }

    public String getStartingNumber(String input) {
        return defineNumber(input);
    }

    /**
     * Method is used to consequently find all the names of numbers
     */
    @Override
    public String getWrittenNumber() {
        return super.getWrittenNumber() + defineNumber(super.inputStr);
    }

    /**
     * Defines the starting number of a digit
     * 
     * @return number
     */
    public String defineNumber(String nr) {
        char[] charArr = nr.toCharArray();
        char ch = charArr[charArr.length - 3];
        switch (ch) {
        case '0':
            number = "";
            break;
        case '1':
            number = " one hundred";
            break;
        case '2':
            number = " two hundred";
            break;
        case '3':
            number = " three hundred";
            break;
        case '4':
            number = " four hundred";
            break;
        case '5':
            number = " five hundred";
            break;
        case '6':
            number = " six hundred";
            break;
        case '7':
            number = " seven hundred";
            break;
        case '8':
            number = " eight hundred";
            break;
        case '9':
            number = " nine hundred";
            break;
        }
        return number;
    }

    @Override
    public String getInputDigits() {
        // TODO Auto-generated method stub
        return null;
    }

}
