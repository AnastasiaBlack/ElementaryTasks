package com.softserve.edu.task5;

public class Thousands extends DigitsDecorator {
    private String number;

    public Thousands(NumberInterface newbaseNumber) {
        super(newbaseNumber);
        inputStr = newbaseNumber.getInputDigits();
    }

    public Thousands() {
    }

    public String getStartingNumber(String input) {
        return defineNumber(input);
    }

    @Override
    public String getWrittenNumber() {
        return super.getWrittenNumber() + defineNumber(inputStr);
    }

    @Override
    public String getInputDigits() {
        // TODO Auto-generated method stub
        return null;
    }

    public String defineNumber(String nr) {
        char[] charArr = nr.toCharArray();
        int lengthCh = charArr.length;
        switch (lengthCh) {
        case 4:
            defineThousandFourDigits(charArr);
            break;
        case 5:
            defineThousandFiveDigits(charArr);
            break;
        case 6:
            defineThousandSixDigits(charArr);
            break;
        }
        return number;
    }
/**
 * Defines the number of thousands
 * @param charArr
 */
    private void defineThousandFourDigits(char[] charArr) {
        char zerCh = charArr[charArr.length - 4];
        switch (zerCh) {
        case '1':
            number = " one thousand";
            break;
        case '2':
            number = " two thousand";
            break;
        case '3':
            number = " three thousand";
            break;
        case '4':
            number = " four thousand";
            break;
        case '5':
            number = " five thousand";
            break;
        case '6':
            number = " six thousand";
            break;
        case '7':
            number = " seven thousand";
            break;
        case '8':
            number = " eight thousand";
            break;
        case '9':
            number = " nine thousand";
            break;
        }
    }
/**
 * Defines a number of thousands if it is a 5 digit number
 * @param charArr
 */
    private void defineThousandFiveDigits(char[] charArr) {
        char zerCh = charArr[charArr.length - 4];
        char tens = charArr[charArr.length - 5];
        StringBuilder sb = new StringBuilder();
        sb.append(tens);
        sb.append(zerCh);
        String tempStr = sb.toString();
        Tens tempTen = new Tens();
        number = tempTen.defineNumber(tempStr) + " thousand";

    }
/**
 * Defines a number of thousands if it is a 6 digit number
 * @param charArr
 */
    private void defineThousandSixDigits(char[] charArr) {
        char zerCh = charArr[charArr.length - 4];
        char tens = charArr[charArr.length - 5];
        char hund = charArr[charArr.length - 6];
        StringBuilder sb = new StringBuilder();
        sb.append(hund);
        sb.append(tens);
        sb.append(zerCh);
        String tempStr = sb.toString();
        DigitsDecorator tempDec = new Tens(new StartingNumber(tempStr, 3));
        tempDec.setInputStr(tempStr);
        number = tempDec.getWrittenNumber() + " thousand";

    }
}
