package com.softserve.edu.task6;

public interface AlgorithmInterface {
    public final int TICKETS_NUMBER = 1_000_000;
    
    
    public boolean isTicketLucky(int ticket);
    public int countTickets();
    public void printOutTickets();
    

}
