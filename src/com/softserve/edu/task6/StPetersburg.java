package com.softserve.edu.task6;

public class StPetersburg implements AlgorithmInterface {
    private final int DIGITS = 6;

    public StPetersburg() {
    }

    @Override
    public boolean isTicketLucky(int ticket) {
        boolean isLucky = false;
        int evenNumber = 0;
        int oddNumber = 0;
        int lastNumber = 0;
        for (int i = 0; i < DIGITS; i++) {
            lastNumber = ticket % 10;
            ticket = (ticket-lastNumber)/10;
            if (lastNumber % 2 == 0) {
                evenNumber += lastNumber;
            } else {
                oddNumber += lastNumber;
            }
        }
        if (evenNumber == oddNumber) {
            isLucky = true;
        }
        return isLucky;
    }

    @Override
    public int countTickets() {
        int count = 0;
        for (int i = 0; i < TICKETS_NUMBER; i++) {
            if (isTicketLucky(i)) {
                count++;
            }
        }
        return count;
    }

    @Override
    public void printOutTickets() {
        System.out.println(countTickets() + " is the number of lucky tickets "
                + "according to the St.Peterburg's algorithm");
    }

}
