package com.softserve.edu.task6;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class AlgChooser {
    private String filePath;
    private final String MOSCOW = "moscow";
    private final String PETER = "piter";

    public AlgChooser(String path) {
        this.filePath = path;
    }

    public String getKeyWordFromFile() {
        File file = new File(filePath);
        String algType = null;
        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(new FileInputStream(file)))) {
            algType = reader.readLine();
        } catch (IOException e) {
            System.out.println("Oops, something went wrong while processing "
                    + "your file");
            e.printStackTrace();
        }
        String type = algType.toLowerCase();
        return type;
    }

    public AlgorithmInterface getAlgorithm() {
        String keyWord = getKeyWordFromFile();
        AlgorithmInterface algorithm=null;
        switch (keyWord) {
        case MOSCOW:
            algorithm = new MoscowAlgorithm();
            break;
        case PETER:
            algorithm = new StPetersburg();
            break;
        default:
            System.out.println(
                    "Cannot find the proper algorithm to count the tickets."
                    + "Please, check the spelling of the keyword in your file");
        }
        return algorithm;
    }
    
    

}
