package com.softserve.edu.task6;

public class MoscowAlgorithm implements AlgorithmInterface {
    
    public MoscowAlgorithm() {
    }

    
    @Override
    public boolean isTicketLucky(int ticket) {
        boolean isLucky=false;
        int threeLast = 0;
        int threeFirst = 0;
        for (int i = 0; i < 3; i++) {
            int lastNumber = ticket % 10;
            threeLast += lastNumber;
            ticket = (ticket - lastNumber) / 10;
        }
        for (int i = 0; i < 3; i++) {
            int lastNumber = ticket % 10;
            threeFirst += lastNumber;
            ticket = (ticket - lastNumber) / 10;
        }
        if (threeFirst == threeLast) {
            isLucky= true;
        }
        return isLucky;
        
    }

    
    @Override
    public int countTickets() {
        int count = 1; // Because a ticket 000 000 will always be a lucky one
        for (int i = 1001; i < TICKETS_NUMBER; i++) {
            if (isTicketLucky(i)) {
                count++;
            }
        }
        return count;
    }

    
    @Override
    public void printOutTickets() {
        System.out.println(countTickets() + " is the number of lucky tickets "
                + "according to the Moscow algorithm");
        }

}
