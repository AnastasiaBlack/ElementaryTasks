package com.softserve.edu.task6.mock.tests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.softserve.edu.task6.AlgChooser;
import com.softserve.edu.task6.AlgorithmInterface;
import com.softserve.edu.task6.MoscowAlgorithm;

import org.junit.Assert;

@RunWith(MockitoJUnitRunner.class)
public class AlgChooserMockTest {
    @Mock
    AlgChooser mockChooser;

    @Test
    public void GetAlgorithmTest_returnMoscow() {
        // Arrange
        MockitoAnnotations.initMocks(this);
        Mockito.when(mockChooser.getKeyWordFromFile()).thenReturn("moscow");
        // Act
        String expected = mockChooser.getKeyWordFromFile();
        // Assert
        Assert.assertEquals(expected, mockChooser.getKeyWordFromFile());
    }
    
    @Test
    public void CountTicketTest_MoscowAlgorithm() {
        // Arrange
        MockitoAnnotations.initMocks(this);
        Mockito.when(mockChooser.getAlgorithm()).thenReturn(new MoscowAlgorithm());
        // Act
        AlgorithmInterface alg = mockChooser.getAlgorithm();
        int actualCount = alg.countTickets();
        int expectedCount = 55252;
        // Assert
        Assert.assertEquals(expectedCount, actualCount);
    }
}
