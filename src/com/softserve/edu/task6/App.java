package com.softserve.edu.task6;

import java.util.Scanner;

public class App {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Please, enter the path of your file");   
        // src\\com\\softserve\\edu\\task6\\LuckyTicket.txt
        String path = sc.nextLine();
        AlgorithmInterface algorithm = new AlgChooser(path).getAlgorithm();
        algorithm.printOutTickets();
        sc.close();

    }

}
