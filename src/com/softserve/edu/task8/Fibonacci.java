package com.softserve.edu.task8;

import java.math.BigInteger;
import java.security.InvalidParameterException;
import java.util.ArrayList;

import com.softserve.edu.task1.IncorrectInputFormateException;

public class Fibonacci {
    private BigInteger fiboStart = new BigInteger("1");
    private BigInteger temPrev   = new BigInteger("1");
    private String     from;
    private String     to;

    public Fibonacci(String [] args) throws IllegalArgumentException {
        if(args.length<2){
            System.out.println("RULES: Enter the range in a form of two "
                    + "parameters as the arguments before launching the "
                    + "programme.");
            throw new IllegalArgumentException(
                    "A user entered an empty string");
        }
        this.from = args[0];
        this.to = args[1];
        validateInput(from, to);
    }

    public void printOutFibonRange() {
        BigInteger zero = new BigInteger("0");
        BigInteger fibo;
        BigInteger fromNumber = new BigInteger(from);
        BigInteger toNumber = new BigInteger(to);
        ArrayList<BigInteger> fiboList = new ArrayList<BigInteger>();
        boolean active = true;
    
        while (active) {
            fibo = fiboStart.add(temPrev);
            if (fromNumber.equals(fiboStart) || fromNumber.equals(zero)) {
                fiboList.add(fiboStart);
                fiboList.add(temPrev);
            }
            temPrev = fiboStart;
            fiboStart = fibo;
            if (fibo.compareTo(fromNumber) > 0
                    && toNumber.compareTo(fibo) > 0) {
                fiboList.add(fibo);
            }
            if (fibo.compareTo(toNumber) > 0) {
                active = false;
            }
        }
        for (int i = 0; i < fiboList.size(); i++) {
            System.out.print(fiboList.get(i) + ", ");
        }
    }

    public void validateInput(String from, String to) {
        BigInteger fromNumber = new BigInteger(from);
        BigInteger toNumber = new BigInteger(to);
        if (toNumber.compareTo(fromNumber) <= 0) {
            throw new InvalidParameterException(
                    "\n\nUh-ho! You've entered wrong "
                            + "range of numbers.\nSecond parameter should be bigger than "
                            + "the first one\n");
        }

    }

}
