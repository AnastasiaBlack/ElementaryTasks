package com.softserve.edu.task8.tests;

import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.Test;

import com.softserve.edu.task1.IncorrectInputFormateException;
import com.softserve.edu.task8.Fibonacci;

public class FibonacciTest {
    @Mock
    private Fibonacci fiboMock;

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void Fibonacci_ExpectedIncorrectInputFormateExceptionThrown()
            throws IncorrectInputFormateException {
        // Arrange
        String[] args = new String[0];
        // Act
        new Fibonacci(args);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void ValidateInput_ExpectedIllegalArgumentExceptionThrown() {
        // Arrange
        String from = "14";
        String to = "3";
        MockitoAnnotations.initMocks(this);
        Mockito.doThrow(IllegalArgumentException.class).when(fiboMock)
                .validateInput(from, to);
        // Act
        fiboMock.validateInput(from, to);
    }

}
