package com.softserve.edu.task7;

/**
 * Программа выводит ряд натуральных чисел через запятую, квадрат которых меньше
 * заданного n. Программа запускается через вызов главного класса с параметрами.
 */
public class App {

    /**We cast double into integer without a fear of loosing the data because 
     * according to the task we only work with whole numbers. And then we compared
     * int to Integer due to the autoboxing.
     * 
     * @param args
     */
    public static void main(String[] args) {
        int argInt = Integer.valueOf(args[0]);
        for (int i = 1; (int) (Math.pow(i, 2)) < argInt; i++) {
            if ((int) (Math.pow(i + 1, 2)) < argInt) {
                System.out.print(i + ", ");
            } else {
                System.out.print(i + "");

            }
        }

    }

}
