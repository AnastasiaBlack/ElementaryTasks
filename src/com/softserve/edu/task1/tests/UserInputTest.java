package com.softserve.edu.task1.tests;

import static org.junit.Assert.*;

import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.softserve.edu.task1.IncorrectInputFormateException;
import com.softserve.edu.task1.UserInputValidation;

public class UserInputTest {
    @Mock
    private UserInputValidation uivalidator;

    @Test
    public void ValidateInput_ExpectedIncorrectInputFormateExeptionThrown() throws IncorrectInputFormateException {
        MockitoAnnotations.initMocks(this);
        String height = null;
        String width = "23";
        Mockito.doThrow(IncorrectInputFormateException.class).when(uivalidator)
                .validateInput(height, width);
    }
    


}
