package com.softserve.edu.task1;

public class UserInputValidation {
    private int height;
    private int width;

    public UserInputValidation() {
    }

    /**
     * Validates the input string-data and if they have a correct format - turns
     * them into int-type, writing them into the fields height and width
     * 
     * @param heightStr
     * @param widthStr
     * @throws IncorrectInputFormateException
     */

    public void startUsertInput(String heightStr, String widthStr) {
        try {
            validateInput(heightStr, widthStr);
        } catch (IncorrectInputFormateException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();

        }
        height = Integer.parseInt(heightStr);
        width = Integer.parseInt(widthStr);

    }

    /**
     * Checks whether the input strings are not null and correspond to the
     * number.
     * 
     * @param heightStr
     * @param widthStr
     * @throws IncorrectInputFormateException
     *             when the input strings is null or not a number
     */
    public void validateInput(String heightStr, String widthStr)
            throws IncorrectInputFormateException {
        if (heightStr.equals(null) || widthStr.equals(null)) {
            throw new IncorrectInputFormateException(
                    "!!!Enter the height and the width of the chess-board and "
                            + "it will be printed out in the console!!!");

        }
        String regX = "\\d*";
        if (!heightStr.matches(regX) || !widthStr.matches(regX)) {
            throw new IncorrectInputFormateException(
                    "!!! Uh-oh!)Incorrect input formate. Please enter the "
                            + "numbers only !!!");
        }
    }

    /**
     * Generally, it is the launching method that checks whether the arguments
     * were entered by a user at all and if yes - launches the whole
     * chess-building procedure with validations of the input.
     * 
     * @param args
     */
    public void startApp(String[] args) {
        if (args.length == 0) {
            System.out.println(
                    "Enter the height and the width of the chess-board as "
                            + "the arguments during the launch and it will be"
                            + " printed out in the console");
        } else if (args.length == 1) {
            System.out.println(
                    "Uh-oh!You've entered only 1 parameter instead of 2");
        } else if (args.length != 0) {
            try {
                ChessRowBuilder chess = new ChessRowBuilder();
                chess.buildChessBoard(args[0], args[1]);
            } catch (Exception e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        }
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

}
