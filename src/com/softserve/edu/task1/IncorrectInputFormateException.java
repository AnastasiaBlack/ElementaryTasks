package com.softserve.edu.task1;

public class IncorrectInputFormateException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public IncorrectInputFormateException() {
    }

    public IncorrectInputFormateException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    public IncorrectInputFormateException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

    public IncorrectInputFormateException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

    public IncorrectInputFormateException(String message, Throwable cause, boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        // TODO Auto-generated constructor stub
    }

}
