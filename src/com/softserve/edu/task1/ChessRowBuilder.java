package com.softserve.edu.task1;

public class ChessRowBuilder {
    private int height;
    private int width;
    private String[] row1;
    private String[] row2;
    private UserInputValidation userVal;

    public ChessRowBuilder() {
        userVal = new UserInputValidation();
    }

    /**
     * Activates user input and fills in the raws of the chess-board.
     * @param heightStr
     * @param widthStr
     * @throws IncorrectInputFormateException
     */

    public void buildChessBoard(String heightStr, String widthStr) throws IncorrectInputFormateException {
        userVal.startUsertInput(heightStr, widthStr);
        this.height = userVal.getHeight();
        this.width = userVal.getWidth();
        row1 = new String[width];
        row2 = new String[width];
        for (int m = 0; m < height; m++) {
            fillRow(m);
        }
    }

    /**
     * Prints out a raw (one at a time) of the future chess-board, analyzing
     * what type of the raw will be the next (starting with * or a whitespace)
     * 
     * @param rowNumber
     */

    public void fillRow(int rowNumber) {
        if (rowNumber % 2 == 0) {
            for (int i = 0; i < width; i += 2) {
                row1[i] = "*";
                if (i != width - 1) {
                    row1[i + 1] = " ";
                }
                System.out.print(row1[i]);
                if (i != width - 1) {
                    System.out.print(row1[i + 1]);
                }
            }

        } else {
            System.out.println();
            for (int i = 0; i < width; i += 2) {
                row2[i] = " ";
                if (i != width - 1) {
                    row2[i + 1] = "*";
                }

                System.out.print(row2[i]);
                if (i != width - 1) {
                    System.out.print(row2[i + 1]);
                }

            }
            System.out.println();

        }

    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }
}
